# kgc/ut2004 - v0.1.0a
#   https://gitlab.com/kendallcon/kgc/ut2004
#
# Unreal Tournament 2004 server container image.
# Server has latest patch (3369.2), Epic ECE Bonus Pack, and Bonus Megapack.
#
FROM alpine:edge
WORKDIR /app
