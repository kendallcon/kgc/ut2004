# kgc/**ut2004**
[![](https://badgen.net/badge/icon/kendallcon%2f?label&icon=gitlab&labelColor=555&color=556)](https://gitlab.com/kendallcon) 
[![](https://badgen.net/badge/icon/kgc%2fut2004?label&labelColor=555&color=D97)](https://gitlab.com/kendallcon/kgc/ut2004) 
[![](https://badgen.net/badge/version/0.2.0a?labelColor=555&color=86B)](https://gitlab.com/kendallcon/kgc/ut2004/releases)  
<!--[![](https://badgen.net/badge/icon/registry.gitlab.com%2fkendallcon%2fkgc%2fut2004:latest?label&icon=docker&labelColor=555&color=68C)](https://gitlab.com/kendallcon)-->

Containerized [*Unreal Tournament 2004*](#) game server.  
Part of the [**KGC** Project](https://gitlab.com/kendallcon/kgc/project) series of container images.

## Deploying

### Running Directly
A basic test run directly via Docker without any environment variable overrides, using a locally-built image as described in *Building*, above, would look like the following:

```bash
docker run -t -p 3000:3000 -d kgc/ut2004:local
```

You should now be able to connect to the game server using the IP of the Docker host machine, provided your firewall has not blocked access to the default port of `64640`.

### Running Via Compose
Navigate to the location you plan on keeping the container definition repository in.  
Clone and enter the repository with:
```bash
git clone https://gitlab.com/kendallcon/kgc/ut2004 && cd ut2004
```

The default `docker-compose.yml` included in the repository will run an instance of **`kgc/ut2004`** by pulling the latest stable image build from the repository registry (`kgc/ut2004:latest`).

Make a copy of the `.env.template` file as `.env`:
```bash
cp .env.template .env
```

Now, bring up the Compose project to start the server:

```bash
docker compose up -d
```

The server should now be running. You can check with `docker compose ps`. Watch logs with `docker compose logs` (Use the `-f` flag to follow the log.)

You may wish to add certain labels or configuration options. A semi-preconfigured alternative compose file to use a central *Traefik* configuration will be added in the future.

## Building

### With Utility Script
```bash
./util/build.sh
```

This will build `kgc/ut2004:latest` if you are in the `main` branch, or `kgc/ut2004:dev-latest` if you are in the `dev` branch.

You can specify the `local` flag when calling the build script ("`./util/build.sh local`") to build `kgc/ut2004:local`; this will also be the default build tag if you call the script from outside of the `main` or `dev` branch of the Git repository.

### Manually
Assuming a modern Docker CE installation, run the following within the root of the repository:

```bash
docker build -t kgc/ut2004:local .
```

Using the `:local` tag is recommended when working with code outside Git branches `main` or `dev`, as well as when working experimentally.

## Configuration

### Runtime Environment Variables

The entirety of the game server configuration can be set via environment variables.  
If using Compose to run the server, ensure you have copied the `.env.template` file to `.env` and edit the configuration variables accordingly before you bring up the Compose project.

| Variable | Default | Usage |
|----------|---------|-------|
| `PB_CFG_LAN` | `true` | Server is operating in LAN mode. Recommended to not change. |